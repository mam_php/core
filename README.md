## Make sure

1. You have traefik reverse proxy running
2. Your reverse proxy and this application using the same docker network, in my case both using network web
3. You have setup your local host 
> 0.0.0.0         php-test.mam.local

## How to start the server 

> cd [to relative path of code base]

> docker-compose up -d && docker exec -i php-test  php -S 0.0.0.0:80

### Access the application on 

> http://php-test.mam.local