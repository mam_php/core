<?php

class Product {

    protected $sku;

    public function getSku(){

        return 'apple';

    }

    public function setSku($sku){

        $this->sku = $sku;

    }
}

try {
    $rc = new ReflectionClass('Product');
    $rm = new ReflectionMethod('Product', 'getSku');
    $rp = new ReflectionProperty('Product', 'sku');
    $rparam = new ReflectionParameter(['Product','setSku'], 0);

    echo '<pre>' . print_r(get_class_methods($rparam), true);

}catch (\Exception $e){

    echo $e->getMessage();

}